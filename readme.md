<div style="text-align:center" markdown="1">
![](dev/readme_logo.png)
</div>

## 1. Introducción ##

*Air Hockey* es un simple juego desarrollado con [OpenFL](http://www.openfl.org/ "OpenFl") para cualquier plataforma (principalmente, ordenadores). Un ejemplo bastante sencillo de su implementación.

## 2.Librerías y dependencias ##

Se han utilizado las siguientes librerías de [Haxe](http://haxe.org/ "Haxe"):

- [OpenFL](http://www.openfl.org/ "OpenFl"): Juego en sí.
- [Actuate](http://lib.haxe.org/p/actuate "Actuate"): Animaciones. <code>Sin usar</code>
- [PhysAxe](http://old.haxe.org/com/libs/physaxe "PhysAxe"): Parte de física y simulaciones.

## 3. Implementación ##

Se utilizará Haxe como lenguaje de alto nivel, traduciendo como target a *cpp* y compilando con *MinGW*.

## 6. Construcción del proyecto ##

Use el entorno [FlashDevelop](http://www.flashdevelop.org/ "FlashDevelop") para abrir, construir y ejecutar el proyecto como si de cualquier otro se tratase.

Para compilarlo de manera manual, use en una terminal, teniendo [Lime](http://lib.haxe.org/p/lime "Lime") instalado:

````
  lime test <target>
````

Por ejemplo:

````
  lime test html5
````

## 5. Autores ##

Este proyecto ha sido desarrollado, en conjunto, por:

| Avatar  | Nombre        | Nickname  | Correo electrónico |
| ------- | ------------- | --------- | ------------------ |
| ![](http://i59.tinypic.com/fasosx.jpg)  | Daniel Herzog | Wikiti | [wikiti.doghound@gmail.com](mailto:wikiti.doghound@gmail.com)