/*******core****************************************************
 * Capítulo 3. Tutorial de desarrollo con OpenFL. Bucle de juego
 * Programación Multimedia y Juegos
 * Autor: David Vallejo Fernández    david.vallejo@openflbook.com
 * Autor: Carlos González Morcillo   carlos.gonzalez@openflbook.com
 * Autor: David Frutos Talavera      david.frutos@openflbook.com
 *
 * You can redistribute and/or modify this file under the terms of the
 * GNU General Public License ad published by the Free Software
 * Foundation, either version 3 of the License, or (at your option)
 * and later version. See <http://www.gnu.org/licenses/>.
 *
 * This file is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.  
 *********************************************************************/

package core;

import openfl.display.Stage;
import openfl.display.StageAlign;
import openfl.display.StageQuality;
import openfl.display.StageScaleMode;

import openfl.events.Event;
import openfl.events.TouchEvent;

import motion.Actuate; 

import openfl.events.KeyboardEvent; 
import openfl.events.MouseEvent;

import openfl.Lib;

class GameManager {

  // Variable estática para implementar Singleton.
  public static var _instance:GameManager;
  // Pila de estados.
  private var _states:Array<GameState>;

  private function new () { 
    _states = new Array<GameState>();
  }

  // Patrón Singleton.
  public static function getInstance() : GameManager {
    if (GameManager._instance == null)
      GameManager._instance = new GameManager();
    return GameManager._instance;
  }
  
  public static function stage(): Stage {
    return Lib.current.stage;
  }

  public function start (state:GameState) : Void {
    Lib.current.stage.align = StageAlign.TOP_LEFT;
    Lib.current.stage.scaleMode = StageScaleMode.NO_SCALE;

    // Registro de event listeners.
    // Permiten asociar evento y código de tratamiento.
    Lib.current.stage.addEventListener(Event.ENTER_FRAME, frameStarted);
    //Lib.current.stage.addEventListener(Event.EXIT_FRAME, frameEnded);
#if !mobile
    Lib.current.stage.addEventListener(MouseEvent.CLICK, mouseEvent);
    Lib.current.stage.addEventListener(MouseEvent.MOUSE_MOVE, mouseEvent);
    // ...
    Lib.current.stage.addEventListener(KeyboardEvent.KEY_DOWN, keyEvent);
    Lib.current.stage.addEventListener(KeyboardEvent.KEY_UP, keyEvent);
#else
    Lib.current.stage.addEventListener(TouchEvent.TOUCH_BEGIN, touchEvent);
    // ...
#end

    // Transición al estado inicial.
    changeState(state);
  }

  public function changeState (state:GameState) : Void { 
    // Limpieza del estado actual.
    if (_states.length > 0) {
      _states.pop().exit();
    }
    
    // Transición al nuevo estado.
    _states.push(state);
    state.enter();
  }

  public function pushState (state:GameState) : Void { 
    // Pause del estado actual.
    if (_states.length > 0) {
      _states[_states.length -1].pause();
    }

    // Transición al nuevo estado.
    _states.push(state);
    state.enter();
  }

  public function popState () : Void {  
    // Limpieza del estado actual.
    if (_states.length > 0) {
      _states.pop().exit();
    }

    // Reanudación del estado anterior.
    _states[_states.length -1].resume();
  }

  // Gestión de eventos de teclado y ratón.
  // Se delega en el estado actual (cima de la pila).

#if !mobile
  public function keyEvent (event:KeyboardEvent) : Void {
    _states[_states.length -1].keyEvent(event);
  }

  public function mouseEvent (event:MouseEvent) : Void { 
    _states[_states.length -1].mouseEvent(event);
  } 
#else
  public function touchEvent (event:TouchEvent) : Void { 
    _states[_states.length -1].touchEvent(event);
  }
#end
  // Gestión básica para la gestión
  // de eventos antes y después de renderizar un frame.

  public function frameStarted (event:Event) : Void { 
    _states[_states.length -1].frameStarted(event);
  }

  public function frameEnded (event:Event) : Void { 
    _states[_states.length -1].frameEnded(event);
  }

}