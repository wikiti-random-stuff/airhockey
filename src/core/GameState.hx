/*********************************************************************
 * Capítulo 3. Tutorial de desarrollo con OpenFL. Bucle de juego
 * Programación Multimedia y Juegos
 * Autor: David Vallejo Fernández    david.vallejo@openflbook.com
 * Autor: Carlos González Morcillo   carlos.gonzalez@openflbook.com
 * Autor: David Frutos Talavera      david.frutos@openflbook.com
 *
 * You can redistribute and/or modify this file under the terms of the
 * GNU General Public License ad published by the Free Software
 * Foundation, either version 3 of the License, or (at your option)
 * and later version. See <http://www.gnu.org/licenses/>.
 *
 * This file is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.  
 *********************************************************************/

package core;

import openfl.display.Sprite;

import openfl.events.Event;
import openfl.events.TouchEvent;
import openfl.events.KeyboardEvent;
import openfl.events.MouseEvent;

class GameState extends Sprite {
  
  // Constructor privado (clase abstracta).

  private function new () {
    super();
  }
  
  // Gestión básica del estado.

  public function enter  () : Void { }
  public function exit   () : Void { }
  public function pause  () : Void { }
  public function resume () : Void { }

  // Gestión básica de eventos de teclado y ratón.

#if !mobile
  public function keyEvent    (event:KeyboardEvent) : Void { }
  public function mouseEvent  (event:MouseEvent)    : Void { }
#else
  public function touchEvent    (event:TouchEvent) : Void { }
#end

  // Gestión básica para el manejo
  // de eventos antes y después de renderizar un frame.

  public function frameStarted (event:Event) : Void { }
  public function frameEnded   (event:Event) : Void { }
	
  // Gestión básica de transiciones entre estados.
  // Se delega en el GameManager.

  public function changeState (state:GameState) : Void { 
    GameManager.getInstance().changeState(state);
  }

  public function pushState (state:GameState) : Void { 
    GameManager.getInstance().pushState(state);
  }

  public function popState () : Void {  
    GameManager.getInstance().popState();
  }
  
}