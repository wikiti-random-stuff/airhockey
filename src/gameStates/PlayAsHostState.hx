package gameStates;

import openfl.display.Sprite;
import openfl.display.StageAlign;
import openfl.display.StageScaleMode;
import openfl.display.Bitmap;

import openfl.media.Sound;
import openfl.media.SoundChannel;

import openfl.text.TextField;
import openfl.text.TextFormat;
import openfl.text.TextFieldAutoSize;
import openfl.events.*;

import openfl.ui.Keyboard;
import openfl.ui.Mouse;

import openfl.Lib;
import openfl.Assets;

import motion.Actuate;
import motion.easing.*;

import phx.col.AABB;
import phx.col.SortedList;
import phx.Vector;
import phx.World;
import phx.Shape;
import phx.FlashDraw;

import objects.WorldDimensions;
import objects.host.Disk_Host;
import objects.host.Pad_Host_Player1;
import objects.host.Goal_Host;
import objects.host.Score_Host;

import core.SoundManager;
import core.GameState;
import core.GameManager;

//import openfl.net.Socket;

class PlayAsHostState extends GameState
{
  private static var s_instance: PlayAsHostState;
  
  private var m_img_board: Bitmap;
  private var m_pad_player1: Pad_Host_Player1;
  private var m_disk: Disk_Host;
  private var m_score: Score_Host;
  private var m_goal_player1: Goal_Host;
  private var m_goal_player2: Goal_Host;

  private var m_phys_world: World;

  private function new() {
    super();
    
    // Capa de dibujado
    
    // Fondo
    m_img_board = new Bitmap( Assets.getBitmapData("img/board.png") );
    m_img_board.scaleX = m_img_board.scaleY = 1 / 2.5;
    
    // Pads
    m_pad_player1 = new Pad_Host_Player1();
    
    // Disk
    m_disk = new Disk_Host();
    
    // Score
    m_score = new Score_Host(WorldDimensions.BoardWidth() / 2.0, 30, "fonts/8bit16.ttf", 0x04E543);
    
    // Música
    //SoundManager.getInstance().loadBackgroundMusic(_snd_file_background);
  }
  
  private function createWorld() {
    // Crear espacio del mundo
    var world_size = new AABB( -1500, -1500, 1500, 1500);
    var broadphase = new SortedList();
    m_phys_world = new World(world_size, broadphase);
    
      // Crear límites
    // Superior
    m_phys_world.addStaticShape( Shape.makeBox( WorldDimensions.BoardWidth() - WorldDimensions.OffsetLeft() - WorldDimensions.OffsetRight(),      // Ancho
                                                WorldDimensions.OffsetTop(),                                                                      // Alto
                                                WorldDimensions.OffsetLeft(), 0) );                                                               // X, Y
    // Inferior
    m_phys_world.addStaticShape( Shape.makeBox( WorldDimensions.BoardWidth() - WorldDimensions.OffsetLeft() - WorldDimensions.OffsetRight(),      // Ancho
                                                WorldDimensions.OffsetBottom(),                                                                   // Alto
                                                WorldDimensions.OffsetLeft(), WorldDimensions.BoardHeight() - WorldDimensions.OffsetBottom() ) ); // X, Y

    // Izquierdo por encima de la porteria
    m_phys_world.addStaticShape( Shape.makeBox( WorldDimensions.OffsetLeft(),                                                                    // Ancho
                                                WorldDimensions.BoardHeight() / 2.0 - WorldDimensions.GoalHeight() / 2.0,                         // Alto
                                                0, 0 ));                                                                                          // X, Y

    // Izquierdo por debajo de la porteria
    m_phys_world.addStaticShape( Shape.makeBox( WorldDimensions.OffsetLeft(),                                                                    // Ancho
                                                WorldDimensions.BoardHeight() / 2.0 - WorldDimensions.GoalHeight() / 2.0,                         // Alto
                                                0, WorldDimensions.BoardHeight() / 2.0 + WorldDimensions.GoalHeight() / 2.0 ));                   // X, Y
                                                
    // Portería izquierda
    m_goal_player1 = new Goal_Host(m_phys_world, 0);
    
    // Derecho por encima de la portería
    m_phys_world.addStaticShape( Shape.makeBox( WorldDimensions.OffsetRight(),                                                                    // Ancho
                                                WorldDimensions.BoardHeight() / 2.0 - WorldDimensions.GoalHeight() / 2.0,                         // Alto
                                                WorldDimensions.BoardWidth() - WorldDimensions.OffsetRight(), 0 ));     // X, Y

    // Derecho por debajo de la portería
    m_phys_world.addStaticShape( Shape.makeBox( WorldDimensions.OffsetRight(),                                                                    // Ancho
                                                WorldDimensions.BoardHeight() / 2.0 - WorldDimensions.GoalHeight() / 2.0,                         // Alto
                                                WorldDimensions.BoardWidth() - WorldDimensions.OffsetRight(),                                     // X
                                                WorldDimensions.BoardHeight() / 2.0 + WorldDimensions.GoalHeight() / 2.0 ));                      // Y
    
    // Portería derecha
    m_goal_player2 = new Goal_Host(m_phys_world, WorldDimensions.BoardWidth() - WorldDimensions.GoalWidth());
  }
  
  public static function getInstance():PlayAsHostState {
    if (PlayAsHostState.s_instance == null)
      PlayAsHostState.s_instance = new PlayAsHostState();
    return PlayAsHostState.s_instance;
  }
  
  override function enter  () : Void {
    Lib.current.stage.align = StageAlign.TOP_LEFT;
    Lib.current.stage.scaleMode = StageScaleMode.NO_SCALE;
    
    // Esconder ratón
    //Mouse.hide();
    
    // Physics
    createWorld();
    
    // Board
    m_img_board.x = 0;
	  m_img_board.y = 0;
    addChild(m_img_board);
    
    // Pads
    m_pad_player1.start(m_phys_world);
    addChild(m_pad_player1);
    
    // Disk
    m_disk.start(m_phys_world);
    addChild(m_disk);
    
    // Score
    addChild(m_score);
    
    SoundManager.getInstance().playBackgroundMusic();
  }
  
  override function exit() : Void {
    // Mostrar el ratón
    Mouse.show();
    
    // Board
    removeChild(m_img_board);
    
    // Pads
    m_pad_player1.close();
    removeChild(m_pad_player1);
    
    // Disk
    m_disk.close();
    removeChild(m_disk);
    
    // Score
    removeChild(m_score);
    
    SoundManager.getInstance().stopBackgroundMusic();
  }

  // Pause la animación de los círculos creados.
  override function pause() : Void {
    SoundManager.getInstance().pauseBackgroundMusic();
  }

  // Reanuda la animación de los círculos creados.
  override function resume() : Void {
    SoundManager.getInstance().playBackgroundMusic();
  }

  // Gestión básica de eventos de teclado y ratón.
#if !mobile
  override function keyEvent    (event:KeyboardEvent) : Void {
    if (event.type == KeyboardEvent.KEY_DOWN && event.keyCode == Keyboard.ESCAPE) {
      GameManager.getInstance().pushState(PauseState.getInstance());
    }
  }
  override function mouseEvent  (event:MouseEvent)    : Void {
    m_pad_player1.mouseEvent(event);

  }
#else
  override function touchEvent  (event:TouchEvent) : Void {
  }
#end
  
  //private static var s_elapsed_time: Float;

  override function frameStarted (event:Event) : Void {
    m_pad_player1.phys_update();
    m_disk.phys_update();

    m_phys_world.step(1, 100);
    //m_phys_world.step( (Lib.getTimer() / 1000.0 - s_elapsed_time)*60, Std.int( (Lib.getTimer() / 1000.0 - s_elapsed_time)*40) + 20);  // Usar diferencia de tiempo
    //s_elapsed_time = Lib.getTimer()/1000.0;
    
    m_disk.update();
    
    checkScore();

    // Debug
    /*this.removeChildren();
    var g = Lib.current.graphics;
    g.clear();
    var fd = new FlashDraw( g );
    fd.drawCircleRotation = true;
    fd.drawWorld(m_phys_world);*/
  }
  
  private static var s_last_collision: Int;
  private static inline var LAST_COLLISION_MIN_TIME: Int = 100;
  
  private function checkScore(): Void {
    if (Lib.getTimer() < s_last_collision + LAST_COLLISION_MIN_TIME)
      return;

    if (m_disk.checkCollision(m_goal_player1.getID())) {
      m_score.addScorePlayer2();
      m_disk.resetPosition();
      
      s_last_collision = Lib.getTimer();
    }
    // Suponemos que el disco no es un gato de schrodinger y no puede estar en ambas porterías al mismo tiempo
    else if (m_disk.checkCollision(m_goal_player2.getID())) {
      m_score.addScorePlayer1();
      m_disk.resetPosition();
      
      s_last_collision = Lib.getTimer();
    }
  }
}