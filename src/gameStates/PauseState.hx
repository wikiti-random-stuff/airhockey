package gameStates;

import openfl.display.Bitmap;
import openfl.display.StageAlign;
import openfl.display.StageScaleMode;

import openfl.events.KeyboardEvent;
import openfl.events.Event;

import openfl.ui.Keyboard;

import openfl.Lib;
import openfl.Assets;

import core.GameState;
import core.GameManager;

/**
 * ...
 * @author Daniel Herzog
 */
class PauseState extends GameState
{
  public static var _instance: PauseState;
  
	//var m_paused_logo: Bitmap;

  private function new() {
    super();
    
    // Logo de pausa
    ///paused_logo = new Bitmap( Assets.getBitmapData("img/pause.png") );
  }
  
  public static function getInstance():PauseState {
    if (PauseState._instance == null)
      PauseState._instance = new PauseState();
    return PauseState._instance;
  }
  
  override function enter  () : Void {
    Lib.current.stage.align = StageAlign.TOP_LEFT;
    Lib.current.stage.scaleMode = StageScaleMode.NO_SCALE;
    
    /*paused_logo.x = (Lib.current.stage.stageWidth - paused_logo.width) / 2;
    paused_logo.y = (Lib.current.stage.stageHeight - paused_logo.height) / 2;
    addChild(paused_logo);*/
    
  }
  
  override function exit() : Void { 
    //removeChild(paused_logo);
  }

#if !mobile
  override function keyEvent(event:KeyboardEvent) : Void {
    if (event.type == KeyboardEvent.KEY_DOWN && event.keyCode == Keyboard.SPACE) {
      GameManager.getInstance().popState();
    }
  }
#end
}