package ;

import core.GameManager;
import core.SoundManager;

import gameStates.PlayAsHostState;
import gameStates.PauseState;

import openfl.display.Sprite;

import openfl.Lib;

/**
 * ...
 * @author Daniel Herzog
 */

class Main extends Sprite
{
  public static var _instance: Main;
  
  private function new() {
    super();
    
    SoundManager.getInstance().setVolume(0.25);
    
    addChild(PlayAsHostState.getInstance());
    addChild(PauseState.getInstance());
    
    GameManager.getInstance().start(PlayAsHostState.getInstance());
  }
  
  public static function getInstance() : Main {
    if (Main._instance == null)
      Main._instance = new Main();
    return Main._instance;
  }
  
	public static function main() {
    Lib.current.addChild (Main.getInstance());
	}
}