package objects.host;

import openfl.display.Bitmap;

import phx.World;
import phx.Shape;

import objects.WorldDimensions;

/**
 * ...
 * @author Daniel Herzog
 */
class Goal_Host
{
  private var m_id: Int;
  
  // ¿Añadir un sprite negro y tapar el disco?
  //private var m_img_black: Bitmap;
  
  public function new(world: World, x_pos: Float) {
    var s: Shape = Shape.makeBox( WorldDimensions.GoalWidth(), WorldDimensions.GoalHeight(), x_pos,  WorldDimensions.BoardHeight() / 2.0 - WorldDimensions.GoalHeight() / 2.0);
    m_id = s.id;
    world.addStaticShape(s);
  }
  
  public function getID(): Int {
    return m_id;
  }
}