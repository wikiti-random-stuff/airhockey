package objects.host;

import openfl.text.TextField;
import openfl.text.TextFormat;
import openfl.text.TextFieldAutoSize;

import openfl.Assets;

/**
 * ...
 * @author Daniel Herzog
 */
class Score_Host extends TextField
{
  private var m_score_player1: Int;
  private var m_score_player2: Int;
  
  private var m_x: Float;
  private var m_y: Float;
  
  public function new(x: Float, y: Float, font: String = "", color: Int = 0xFFFFFF) {
    super();
    
    this.selectable = false;
    this.embedFonts = true;
    
    this.autoSize = TextFieldAutoSize.LEFT;
	  this.width = 0;
    
    if (font == "") font = "_sans";
    else font = Assets.getFont (font).fontName;
    
    var tf:TextFormat = new TextFormat(font, 42, color, true);
    this.defaultTextFormat = tf;
    
    this.m_x = x;
    this.m_y = y;
   
    updateText();
  }
  
  public function addScorePlayer1() {
    m_score_player1 += 1;
    
    updateText();
  }
  
  public function addScorePlayer2() {
    m_score_player2 += 1;
    
    updateText();
  }
  
  public function updateText() {
    this.htmlText = m_score_player1 + "-" + m_score_player2;
    
    this.x = this.m_x - this.width/2.0;
    this.y = this.m_y - this.height/2.0;
  }
}