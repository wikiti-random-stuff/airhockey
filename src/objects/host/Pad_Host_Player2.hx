package objects.host;

import openfl.display.Sprite;

import openfl.display.Bitmap;

import phx.Body;
import phx.World;

/**
 * ...
 * @author Daniel Herzog
 */
class Pad_Host_Player2 extends Sprite
{
  private static inline var MAX_SPEED: Float = 40.0;
    
  private var m_img_pad: Bitmap;
  
  private var m_radius: Float;
  
  private var m_phys_body: Body;
  private var m_phys_world: World;
  
  // Todo idéntico al jugador uno, salvo que la nueva posición se recoge del cliente, no del ratón
  
  public function new() 
  {
        
  }
    
}