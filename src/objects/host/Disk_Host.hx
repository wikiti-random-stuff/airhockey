package objects.host;

import openfl.display.Bitmap;
import openfl.display.Sprite;

import openfl.events.MouseEvent;

import openfl.Assets;
import openfl.Lib;

import phx.World;
import phx.Body;
import phx.Shape;
import phx.Circle;
import phx.Vector;

import objects.WorldDimensions;

import core.SoundManager;

/**
 * ...
 * @author Daniel Herzog
 */
class Disk_Host extends Sprite
{
  private static inline var MAX_SPEED: Float = 70.0;
  
  private var m_img_disk: Bitmap;
  private var m_phys_body: Body;
  private var m_phys_world: World;
  
  private var m_radius: Float;
  
  public function new() 
  {
    super();
    
    m_img_disk = new Bitmap( Assets.getBitmapData("img/disk.png") );
    m_img_disk.scaleX = m_img_disk.scaleY = 1 / 5.0; 
    m_img_disk.x = - m_img_disk.width / 2.0;
    m_img_disk.y = - m_img_disk.height / 2.0;
    
    m_radius = m_img_disk.width / 2.0;
  }
  
  public function Radius(): Float {
    return m_radius;
  }
  
#if !mobile
  public function mouseEvent(event: MouseEvent): Void {
    //SoundManager.getInstance().playSoundEffect(_snd_file_kitty);
  }
#else
  
  public function touchEvent(e: TouchEvent): Void {    
    if (e.type != TouchEvent.TOUCH_BEGIN || _paused)
      return;
      
    //SoundManager.getInstance().playSoundEffect(_snd_file_kitty);
  }
#end


  public function start(world: World): Void {
    // Sprite
    this.x = WorldDimensions.BoardWidth()  / 2.0;
    this.y = WorldDimensions.BoardHeight() / 2.0;
    
    this.addChild(m_img_disk);
    
    // Physics
    m_phys_world = world;
    m_phys_body = new Body( this.x, this.y );
    m_phys_body.mass = 1;
    
    var c: Circle = new Circle( m_radius, new Vector(0.0, 0.0) );
    c.material.restitution = 0.8;
    c.material.friction = 0.5;
    
    m_phys_body.addShape( c );
    m_phys_body.updatePhysics();
    
    m_phys_world.addBody(m_phys_body);
  }
  
  public function close(): Void {
    this.removeChild(m_img_disk);
    
    m_phys_world.removeBody( m_phys_body );
  }

  public function pause(): Void {

  }

  public function resume(): Void {

  }
  
  public function update(): Void {
    m_phys_body.updatePhysics();
    
    this.x = m_phys_body.x;
    this.y = m_phys_body.y;
    this.rotation = 180 / Math.PI * m_phys_body.a; // Degrees? Radians?
    
    checkCollisions();
  }
  
  private static var s_last_collided_time: Int;
  private static inline var LAST_COLLIDED_TIME_MIN: Int = 200;
  
  private function checkCollisions() {
    // Comprobar colision y reproducir sonido
    if( !m_phys_body.arbiters.isEmpty() && Lib.getTimer() >= s_last_collided_time + LAST_COLLIDED_TIME_MIN) {
      SoundManager.getInstance().playSoundEffect("snd/smack" + Std.int( 1 + Math.random() * 4 ) + ".ogg");
      s_last_collided_time = Lib.getTimer();
    }
  }
  
  public function checkCollision(id: Int): Bool {
    for (a in m_phys_body.arbiters) {
      if (a.s1.id == id || a.s2.id == id)
        return true;
    }
    
    return false;
  }
  
  public function phys_update(): Void {
    m_phys_world.activate(m_phys_body);
    
    while ( m_phys_body.v.length() > MAX_SPEED) {
      m_phys_body.v = m_phys_body.v.mult( 0.9 );
    }

    m_phys_body.updatePhysics();
    m_phys_world.sync(m_phys_body);
    
  }
  
  public function resetPosition(): Void {
    // Sprite
    this.x = WorldDimensions.BoardWidth()  / 2.0;
    this.y = WorldDimensions.BoardHeight() / 2.0;
    
    // Body
    m_phys_body.set(new Vector(this.x, this.y), 0.0, new Vector(0.0, 0.0), 0.0 );
    m_phys_body.updatePhysics();
    m_phys_world.sync(m_phys_body);
  }
}