package objects.host;

import openfl.display.Bitmap;
import openfl.display.Sprite;

import openfl.events.MouseEvent; 

import openfl.Assets;
import openfl.Lib;

import phx.World;
import phx.Body;
import phx.Circle;
import phx.Vector;

/**
 * ...
 * @author Daniel Herzog
 */
class Pad_Host_Player1 extends Sprite
{
  private static inline var MAX_SPEED: Float = 40.0;
    
  private var m_img_pad: Bitmap;
  
  private var m_radius: Float;
  
  private var m_phys_world: World;
  private var m_phys_body: Body;
  
  public function new() 
  {
    super();
    
    m_img_pad = new Bitmap( Assets.getBitmapData("img/rpad.png") );
    m_img_pad.scaleX = m_img_pad.scaleY = 1 / 5.0; 
    m_img_pad.x = - m_img_pad.width / 2.0;
    m_img_pad.y = - m_img_pad.height / 2.0;
    
    m_radius = m_img_pad.width / 2.0;
  }
  
  public function Radius(): Float {
    return m_radius;
  }
    
#if !mobile
  public function mouseEvent(event: MouseEvent): Void {
    if (event.type == MouseEvent.MOUSE_MOVE) {
      var updated: Bool = false;
      // Mover la X
      if ( event.stageX + m_radius < WorldDimensions.BoardWidth() / 2.0 &&
           event.stageX - m_radius > WorldDimensions.OffsetLeft() ) {
        
        this.x = event.stageX;
        updated = true;
      }
      else {
        if ( event.stageX + m_radius >= WorldDimensions.BoardWidth() / 2.0 ) {
          this.x = WorldDimensions.BoardWidth() / 2.0 - m_radius;
          updated = true;
        }
        else if (event.stageX - m_radius <= WorldDimensions.OffsetLeft()) {
          this.x = WorldDimensions.OffsetLeft() + m_radius;
          updated = true;
        }
      }
      
      // Mover la Y
      if ( event.stageY + m_radius < WorldDimensions.BoardHeight() - WorldDimensions.OffsetBottom() &&
           event.stageY - m_radius > WorldDimensions.OffsetTop() ) {
        
        this.y = event.stageY;
        updated = true;
      }
      else {
        if ( event.stageY + m_radius >= WorldDimensions.BoardHeight() - WorldDimensions.OffsetBottom()) {
          this.y = WorldDimensions.BoardHeight() - WorldDimensions.OffsetBottom() - m_radius;
          updated = true;
        }
        else if ( event.stageY - m_radius <= WorldDimensions.OffsetTop() ) {
          this.y = WorldDimensions.OffsetTop() + m_radius;
          updated = true;
        }
      }
    }

    //SoundManager.getInstance().playSoundEffect(_snd_file_kitty);
  }
#else
  
  public function touchEvent(e: TouchEvent): Void {    
    if (e.type != TouchEvent.TOUCH_BEGIN || _paused)
      return;
      
    //SoundManager.getInstance().playSoundEffect(_snd_file_kitty);
  }
#end


  public function start(world: World): Void {
    // Sprite
    this.x = WorldDimensions.BoardWidth() * 1 / 4.0;
    this.y = WorldDimensions.BoardHeight() / 2.0;
    
    this.addChild(m_img_pad);
    
    // Physics
    m_phys_world = world;
    m_phys_body = new Body( this.x, this.y );
    m_phys_body.mass = 0; // Masa infinita

    var c: Circle = new Circle( m_radius, new Vector(0.0, 0.0) );
    c.material.restitution = 0.0;
    c.material.friction = 1.0;

    m_phys_body.addShape( c );
    m_phys_body.updatePhysics();
    
    m_phys_world.addBody(m_phys_body);

  }
  
  public function close(): Void {
    this.removeChild(m_img_pad);
    
    m_phys_world.removeBody(m_phys_body);
  }

  public function pause(): Void {

  }

  public function resume(): Void {

  }
  
  public function phys_update(): Void {
    m_phys_world.activate(m_phys_body);
        
    var vx = ( this.x - m_phys_body.x) * 1.0;
    var vy = (this.y - m_phys_body.y) * 1.0;
    m_phys_body.setSpeed( vx, vy);
    
    while ( m_phys_body.v.length() > MAX_SPEED) {
      m_phys_body.v = m_phys_body.v.mult( 0.9 );
    }
    
    m_phys_body.w = 0;

    m_phys_body.updatePhysics();
    m_phys_world.sync(m_phys_body);
    
  }
}