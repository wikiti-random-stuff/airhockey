package objects;

/**
 * ...
 * @author Daniel Herzog
 */
class WorldDimensions
{
  private static var s_instance: WorldDimensions;
  
  private static var s_scale: Float = 1 / 2.5;
  
  private static var s_board_width: Float = 3000 * s_scale;
  private static var s_board_height: Float = 1500 * s_scale;
  
  private static var s_offset_top: Float = 161 * s_scale;
  private static var s_offset_bottom: Float = 161 * s_scale;
  private static var s_offset_left: Float = 321 * s_scale;
  private static var s_offset_right: Float = 321 * s_scale;
  
  private static var s_goal_height: Float = 546 * s_scale;
  private static var s_goal_width: Float = 190 * s_scale;
  
  private function new() 
  {
    // ...  
  }
  
  public function getInstance(): WorldDimensions {
    if (s_instance == null)
      return new WorldDimensions();
    return s_instance;
  }
  
  public static function BoardWidth(): Float {
    return s_board_width;
  }
  
  public static function BoardHeight(): Float {
    return s_board_height;
  }
  
  public static function OffsetTop(): Float {
    return s_offset_top;
  }
  
  public static function OffsetBottom(): Float {
    return s_offset_top;
  }
  
  public static function OffsetLeft(): Float {
    return s_offset_left;
  }
  
  public static function OffsetRight(): Float {
    return s_offset_right;
  }
  
  public static function GoalHeight(): Float {
    return s_goal_height;
  }
  
  public static function GoalWidth(): Float {
    return s_goal_width;
  }
}